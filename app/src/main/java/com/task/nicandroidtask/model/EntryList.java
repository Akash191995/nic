package com.task.nicandroidtask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class EntryList extends RealmObject {
    @SerializedName("API")
    @Expose
    private String aPI;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Auth")
    @Expose
    private String auth;
    @SerializedName("HTTPS")
    @Expose
    private Boolean hTTPS;
    @SerializedName("Cors")
    @Expose
    private String cors;
    @SerializedName("Link")
    @Expose
    private String link;
    @SerializedName("Category")
    @Expose
    private String category;

    public static final String COLUMN_ID = "id";
    public static final String TABLE_NAME = "entry";
    public static final String API = "api";
    public static final String Description = "desc";
    public static final String Auth = "auth";
    public static final String HTTPS = "https";
    public static final String Cors = "cors";
    public static final String Link = "link";
    public static final String Category = "category";
    public static final String COLUMN_TIMESTAMP = "timestamp";


    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + API + " TEXT,"
                    + Description + " TEXT,"
                    + Auth + " TEXT,"
                    + HTTPS + " TEXT,"
                    + Cors + " TEXT,"
                    + Link + " TEXT,"
                    + Category + " TEXT,"
                    + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";


    public EntryList() {

    }

    public EntryList(String aPI, String description, String auth, Boolean hTTPS, String cors, String link, String category) {
        this.aPI = aPI;
        this.description = description;
        this.auth = auth;
        this.hTTPS = hTTPS;
        this.cors = cors;
        this.link = link;
        this.category = category;
    }


    public String getaPI() {
        return aPI;
    }

    public void setaPI(String aPI) {
        this.aPI = aPI;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public Boolean gethTTPS() {
        return hTTPS;
    }

    public void sethTTPS(Boolean hTTPS) {
        this.hTTPS = hTTPS;
    }

    public String getCors() {
        return cors;
    }

    public void setCors(String cors) {
        this.cors = cors;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "EntryList{" +
                "aPI='" + aPI + '\'' +
                ", description='" + description + '\'' +
                ", auth='" + auth + '\'' +
                ", hTTPS=" + hTTPS +
                ", cors='" + cors + '\'' +
                ", link='" + link + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
