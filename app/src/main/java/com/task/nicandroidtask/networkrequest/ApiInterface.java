package com.task.nicandroidtask.networkrequest;

import com.task.nicandroidtask.model.EntityModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("entries")
    Call<EntityModel> getEntityData(@Query("category") String category,
                                    @Query("https") String https,
                                    @Query("Auth") String Auth);
}
