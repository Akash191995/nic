package com.task.nicandroidtask.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Entity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.task.nicandroidtask.R;
import com.task.nicandroidtask.adapter.MainAdapter;
import com.task.nicandroidtask.helper.DatabaseHelper;
import com.task.nicandroidtask.helper.InternetCheck;
import com.task.nicandroidtask.helper.ProgressDialog;
import com.task.nicandroidtask.helper.RealmHelper;
import com.task.nicandroidtask.model.EntityModel;
import com.task.nicandroidtask.model.EntryList;
import com.task.nicandroidtask.networkrequest.ApiClient;
import com.task.nicandroidtask.networkrequest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ApiInterface apiService;
    Context context;
    RecyclerView rvEntryList;
    List<EntryList> entryLists=new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    MainAdapter mainAdapter;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=MainActivity.this;
        apiService = ApiClient.getClient().create(ApiInterface.class);

        db = new DatabaseHelper(this);

        rvEntryList=findViewById(R.id.rv_entry_list);

        layoutManager = new LinearLayoutManager(context);
        rvEntryList.setLayoutManager(layoutManager);

        mainAdapter = new MainAdapter(context, entryLists);
        rvEntryList.setAdapter(mainAdapter);

        getEntityData();

    }

    private void getEntityData() {
        new InternetCheck(new InternetCheck.Consumer() {
            @Override
            public void accept(Boolean internet) {
                if (internet) {
                    ProgressDialog.showProgressDialog(context, "Please wait...");
                    Call<EntityModel> call = apiService.getEntityData("Music","true","apiKey");
                    call.enqueue(new Callback<EntityModel>() {
                        @Override
                        public void onResponse(Call<EntityModel> call, Response<EntityModel> response) {
                            entryLists.clear();
                            switch (response.code()) {
                                case 200:
                                    Log.d("getEntityData",response.body().getEntryLists()+"");
                                    if(response.body().getEntryLists()!=null){
                                        entryLists.addAll(response.body().getEntryLists());
                                        mainAdapter.notifyDataSetChanged();
                                        RealmHelper.REALM.beginTransaction();
                                        RealmHelper.REALM.copyToRealmOrUpdate(response.body().getEntryLists());
                                        RealmHelper.REALM.commitTransaction();
                                    }
                                    ProgressDialog.hideProgressDialog();
                                    break;
                                case 403:
                                    ProgressDialog.hideProgressDialog();
                                    break;

                                default:
                                    ProgressDialog.hideProgressDialog();
                                    Toast.makeText(context, "failed", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<EntityModel> call, Throwable t) {
                            ProgressDialog.hideProgressDialog();
                            Toast.makeText(context, "failed", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
