package com.task.nicandroidtask.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.task.nicandroidtask.R;
import com.task.nicandroidtask.model.EntryList;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {
    Context context;
    List<EntryList> entryLists;

    public MainAdapter(Context context, List<EntryList> entryLists){
        this.context=context;
        this.entryLists=entryLists;
    }
    @NonNull
    @Override
    public MainAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.main_list_items, parent, false);
        return new MainAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.MyViewHolder holder, int position) {

        holder.tvApi.setText(entryLists.get(position).getaPI());
        holder.tvCategory.setText(entryLists.get(position).getCategory());
        holder.tvAuth.setText(entryLists.get(position).getAuth());
        holder.tvCors.setText(entryLists.get(position).getCors());
        holder.tvdescription.setText(entryLists.get(position).getDescription());
//        holder.tvhttp.setText(entryLists.get(position).gethTTPS());
        holder.tvlink.setText(entryLists.get(position).getLink());

    }

    @Override
    public int getItemCount() {
        return entryLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvhttp,tvlink,tvdescription,tvApi,tvAuth,tvCors,tvCategory;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvhttp=itemView.findViewById(R.id.tv_https);
            tvlink=itemView.findViewById(R.id.tv_link);
            tvdescription=itemView.findViewById(R.id.tv_description);
            tvApi=itemView.findViewById(R.id.tv_api);
            tvAuth=itemView.findViewById(R.id.tv_auth);
            tvCors=itemView.findViewById(R.id.tv_corns);
            tvCategory=itemView.findViewById(R.id.tv_categrory);
        }
    }
}
