package com.task.nicandroidtask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EntityModel {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("entries")
    @Expose
    private List<EntryList> entryLists;

    public EntityModel() {

    }

    public EntityModel(Integer count, List<EntryList> entryLists) {
        this.count = count;
        this.entryLists = entryLists;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<EntryList> getEntryLists() {
        return entryLists;
    }

    public void setEntryLists(List<EntryList> entryLists) {
        this.entryLists = entryLists;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "count=" + count +
                ", entryLists=" + entryLists +
                '}';
    }
}
