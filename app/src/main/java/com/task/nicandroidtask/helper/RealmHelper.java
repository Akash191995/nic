package com.task.nicandroidtask.helper;

import android.content.Context;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmHelper {
    public static Realm REALM;
    public static RealmConfiguration config;

    public static void setUp(Context context) {

        if (REALM != null) {
            if (!REALM.isClosed()) {
                REALM.close();
                REALM = null;
            }
        }
        Realm.init(context);
        config = new RealmConfiguration.Builder().name("NicTask.realm").schemaVersion(0).deleteRealmIfMigrationNeeded()/*migration(new CustomRealmMigration())*/.build();
        Realm.setDefaultConfiguration(config);
        REALM = Realm.getDefaultInstance();
    }

    public static void closeAndDeleteRealm() {
        if (REALM != null || !REALM.isClosed()) {
            REALM.close();
            Realm.deleteRealm(config);
            REALM = null;
        } else {
            Realm.deleteRealm(config);
        }
    }




}
